package exercise.client

import akka.actor.{Actor, ActorLogging}
import exercise.client.Client._
import play.api.libs.ws.WSResponse
import play.api.libs.ws.ning.NingWSResponse

import scala.util.{Success, Try}
import scala.concurrent.duration._

object Client{
  case class HandleResponse(response:Try[WSResponse])
  case object SendRequest
}
class Client(config:ClientConfig, deviceId:String) extends Actor with ActorLogging{

  lazy val client = {
    val builder = new com.ning.http.client.AsyncHttpClientConfig.Builder()
    new play.api.libs.ws.ning.NingWSClient(builder.build())
  }

  import context.dispatcher
  context.system.scheduler.schedule(0 seconds, config.requestIntervalInMillis milliseconds, self, SendRequest)

  def receive = {
    case SendRequest => sendRequest()
    case HandleResponse(Success(response:NingWSResponse)) =>
      log.info(s"device id: ${deviceId} received code: ${response.ahcResponse.getStatusCode}")
  }

  private def sendRequest() = {
    client.url(config.serviceAddress)
      .withQueryString(
        "deviceId" -> deviceId
      ) get() onComplete {
      case res => self ! HandleResponse(res)
    }
  }
}
