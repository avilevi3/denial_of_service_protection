package exercise.client

import scopt.OptionParser

case class ClientConfig(serviceAddress: String="http://localhost:8080",
                        requestIntervalInMillis: Long=1000, numOfClientsToProvision:Int = 5)
object ClientConfig{
  val parser = new OptionParser[ClientConfig]("denial of service protection exercise.client"){
    head("denial of service protection exercise.client" ,"0.0.1")

    opt[String]('a',"service-address")
      .action((v,c)=>c.copy(serviceAddress = v))
      .text("address of remote service to consume")
    opt[Long]('r',"request-interval")
      .action((v,c)=>c.copy(requestIntervalInMillis = v))
      .text("the sleep time between requests")
    opt[Int]('c',"num-of-clients")
      .action((v,c)=>c.copy(numOfClientsToProvision = v))
      .text("number of clients to providsion")

    help("help").text("prints this usage text")
  }

  def apply(args: Seq[String]): ClientConfig = {
    parser.parse(args, ClientConfig()) match {
      case Some(conf) => conf
      case None => throw new Exception("Invalid application arguments")
    }
  }
}
