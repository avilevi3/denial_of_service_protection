package exercise.client

import akka.actor.{ActorSystem, Props}

object ClientApp extends App{
  val system = ActorSystem("exercise-client-system")
  val config = ClientConfig(args)
  Range(0,config.numOfClientsToProvision).foreach{i=>
    system.actorOf(Props(classOf[Client],config, s"deviceId_$i"))
  }
}
