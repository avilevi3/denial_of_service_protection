package exercise.server

/*request*/
case class ProcessRequest(deviceId:String)

/*responses*/
case object RequestProcessed
case object RequestRejected
