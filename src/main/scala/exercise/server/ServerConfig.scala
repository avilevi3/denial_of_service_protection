package exercise.server

import scopt.OptionParser

case class ServerConfig(timeFrameDuration:Long = 5000, requestsThreshold:Int = 5,
                        hostname:String="localhost", port:Int = 8080, listenTimeout:Long = 200)

object ServerConfig{
  val parser = new OptionParser[ServerConfig]("denial of service protection server"){
    head("denial of service protection exercise.client" ,"0.0.1")

    opt[Long]('d',"time-frame-duration")
      .action((v,c)=>c.copy(timeFrameDuration = v))
      .text("diration of time frame")
    opt[Int]('r',"requests-threshold")
      .action((v,c)=>c.copy(requestsThreshold = v))
      .text("device id to use")
    opt[String]('h',"host-name")
      .action((v,c)=>c.copy(hostname = v))
      .text("host name to bind to")
    opt[Int]('p',"port")
      .action((v,c)=>c.copy(port = v))
      .text("port to listen on")

    help("help").text("prints this usage text")
  }

  def apply(args: Seq[String]): ServerConfig = {
    parser.parse(args, ServerConfig()) match {
      case Some(conf) => conf
      case None => throw new Exception("Invalid application arguments")
    }
  }
}
