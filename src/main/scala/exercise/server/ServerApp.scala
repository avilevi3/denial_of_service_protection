package exercise.server

import akka.actor.{ActorSystem, Props}

object ServerApp extends App{
  val system = ActorSystem("denial-of-service-protection")

  val config = ServerConfig(args)

  /*
  1. no custom supervisor created since default behaviour is suitable
  2. can be scaled out by creating a router that manages a pool of worker actors
  */
  val worker = system.actorOf(Props(classOf[DenialOfServiceProtectionWorker], config.timeFrameDuration, config.requestsThreshold))

  HttpAPI(system, worker, config.hostname,config.port, config.listenTimeout)
}