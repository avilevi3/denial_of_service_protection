package exercise.server

import akka.actor.{Actor, ActorLogging}
import exercise.server.DenialOfServiceProtectionWorker.{ClientState, DoScavenging}

import scala.collection.mutable

object DenialOfServiceProtectionWorker{
  case class ClientState(counter: Int, frameStart: Long) {
    def increment() = copy(counter = this.counter + 1)
  }
  case object DoScavenging
}
class DenialOfServiceProtectionWorker(frameDurationInMillis: Long, requestsThreshold: Int) extends Actor with ActorLogging{

  val clientStates = mutable.Map[String, ClientState]()

  def receive = {
    case ProcessRequest(deviceId) =>
      this.clientStates.get(deviceId) match {
        case Some(state) =>
          handleExistingSession(deviceId, state)
        case None =>
          handleNewDevice(deviceId)
      }
    case DoScavenging => doScavenging()
  }

  private def doScavenging() = {
    val now = System.currentTimeMillis()

    this.clientStates.collect{
      case (deviceId,ClientState(_, frameStart)) if !isInFrame(now, frameStart)=> deviceId
    }.foreach{deviceId=>
      log.info(s"device id: $deviceId - cleaning session tracking")
      clientStates.remove(deviceId)}
  }

  private def handleNewDevice(deviceId: String): Unit = {
    log.info(s"device id: $deviceId - creating new session")
    clientStates.put(deviceId, ClientState(1, System.currentTimeMillis()))
    sender() ! RequestProcessed
  }

  private def handleExistingSession(deviceId: String, state: ClientState): Unit = {
    val newState = state.increment()
    val now = System.currentTimeMillis()
    val inFrame = isInFrame(now, newState.frameStart)
    val exceeded = newState.counter > requestsThreshold
    (inFrame,exceeded) match {
      case (false, _) =>
        log.info(s"device id: $deviceId - out of frame, starting a new frame")
        this.clientStates.update(deviceId, ClientState(1,now))
        sender() ! RequestProcessed
      case (true, true) =>
        log.info(s"device id: $deviceId - exceeded threshold")
        this.clientStates.update(deviceId, newState)
        sender() ! RequestRejected
      case (true, false) =>
        log.info(s"device id: $deviceId - is within threshold boundaries")
        this.clientStates.update(deviceId, newState)
        sender() ! RequestProcessed
    }
  }

  private def isInFrame(now:Long, frameStart: Long) = (now - frameStart) <= this.frameDurationInMillis
}
