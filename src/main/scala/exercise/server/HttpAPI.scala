package exercise.server

import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import org.json4s.{DefaultFormats, jackson}

import scala.util.{Failure, Success}

object HttpAPI extends Directives with Json4sSupport {
  implicit val serialization = jackson.Serialization
  implicit val formats = DefaultFormats

  def apply(actorSystem: ActorSystem, worker:ActorRef, host:String, port:Int, httpTimeout:Long):Unit = {

    implicit val system = actorSystem
    implicit val materializer = ActorMaterializer()
    import system.dispatcher

    implicit val timeout = Timeout(httpTimeout, TimeUnit.MILLISECONDS)

    val route =
      get {
        pathSingleSlash {
          parameters('deviceId){deviceId=>
            onComplete(worker ? ProcessRequest(deviceId)){
              case Success(RequestProcessed) => complete(HttpResponse(StatusCodes.OK))
              case Success(RequestRejected) => complete(HttpResponse(StatusCodes.ServiceUnavailable))
              case Failure(t) =>
                // report error (log\remote service)
                complete(HttpResponse(StatusCodes.ServiceUnavailable))
            }
          }
        }
      }

    Http().bindAndHandle(route, host, port)
  }
}

