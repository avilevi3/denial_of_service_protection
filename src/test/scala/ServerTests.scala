import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.TestActorRef
import akka.util.Timeout
import exercise.server.{DenialOfServiceProtectionWorker, ProcessRequest, RequestProcessed, RequestRejected}
import org.scalatest.{Matchers, WordSpecLike}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

class ServerTests extends WordSpecLike with Matchers{
  "DenialOfServiceProtectionWorker" must {
    "reject requests that exceeds threshold" in {
      implicit val system = ActorSystem("test-system")
      import system.dispatcher
      implicit val timeout = Timeout(1000,TimeUnit.SECONDS)

      val threshold = 5
      val worker = TestActorRef(new DenialOfServiceProtectionWorker(1000, threshold))

      val futuresInFrame = Range(0,threshold).map{_=>
        worker ? ProcessRequest("id")
      }
      val inFrameResults = Await.result(Future.sequence(futuresInFrame), 1 seconds)

      val exceededResult = Await.result(worker ? ProcessRequest("id"), 15 minutes)

      inFrameResults should be (Seq(RequestProcessed,RequestProcessed,RequestProcessed,RequestProcessed,RequestProcessed))
      exceededResult should be (RequestRejected)
    }
  }
}
